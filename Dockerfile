FROM python:3.6-alpine

ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY backend/requirements.txt backend/requirements.txt

RUN apk update && \
  apk add \
    bash \
    curl \
    gettext \
    nodejs \
    npm \
    postgresql-libs && \
  apk add --virtual build-deps \
    gcc \
    git \
    jpeg-dev \
    libffi-dev \
    musl-dev \
    postgresql-dev \
    python3-dev \
    zlib-dev && \
  pip install -r backend/requirements.txt --no-cache-dir && \
  apk --purge del build-deps

COPY . .

RUN cd frontend && npm install && node ./node_modules/.bin/gulp

WORKDIR ./backend
RUN ./manage.py compilemessages

EXPOSE 8000

CMD ["./scripts/wait-for-it.sh", "db:5432", "--", "gunicorn", "wsgi:application", "--workers", "1", "--bind", "0.0.0.0:8000", "--reload"]
