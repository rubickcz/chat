var ok = 0
var nok = 0
var pending = 0

function get_messages() {
    var start = Date.now();

    function log(msg) {
        $('.log').prepend(
            '<span class="time">' + (Date.now() - start) +' ms</span>' + msg +'<br>'
        );
    }

    $.ajax({
        url: "/messages/",
        beforeSend: function(data) {
            pending++;
        },
        success: function(data) {
          if (data.length > 0) {
              ok++;
              log(data[0].hostname);
              $('.messages').html('');
              $.each(data, function (index, value){
                  $('.messages').append(value.nick + ': '+ value.text + '<br>');
              })
          }
        },
        complete: function() {
            pending--;
            $('.meter.ok .count').html(ok);
            $('.meter.nok .count').html(nok);
            $('.meter.pending .count').html(pending);
        },
        error: function(xhr, textStatus) {
            nok++;
            var msg;
            if (xhr.status == 0) {
                msg = 'Unreachable';
            } else {
                msg = 'HTTP '+xhr.status;
            }
            log('<span class="error">' + msg + '</span>')
        },
    })
}

interval = setInterval(get_messages, 500);

$('.submit').click(function() {
    $.ajax({
        url: "/messages/",
        method: 'POST',
        data: {
            nick: $('.nick').val(),
            text: $('.text').val(),
        }
    });
    $('form .text').val('');
});

$('.stop').click(function() {
    clearInterval(interval);
});
