const gulp = require('gulp');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const less = require('gulp-less');

const djangoStaticDir = '../backend/static/'

const css = [
    'src/style.less'
]

const scripts = [
    'src/script.js',
]

// CSS
gulp.task('css', function() {
    gulp.src(css)
        .pipe(concat('chat.css'))
        .pipe(less())
        .pipe(cleanCSS())
        .pipe(gulp.dest(djangoStaticDir));
});

// Scripts
gulp.task('scripts', function() {
    gulp.src(scripts)
        .pipe(concat('chat.js'))
        .pipe(uglify())
        .pipe(gulp.dest(djangoStaticDir));
});

// Watch
gulp.task('watch', function() {
    gulp.watch('src/**', ['css', 'scripts']);
});

// Default
gulp.task('default', ['css', 'scripts']);
