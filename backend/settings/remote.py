from .common import *

# STORAGE CONFIGURATION
# ------------------------------------------------------------------------------

INSTALLED_APPS += (
    'storages',
)
AWS_ACCESS_KEY_ID = secrets['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = secrets['AWS_SECRET_ACCESS_KEY']
AWS_QUERYSTRING_AUTH = False
AWS_S3_ENDPOINT_URL = 'https://ams3.digitaloceanspaces.com'

STATICFILES_STORAGE = 'settings.storages.StaticStorage'
