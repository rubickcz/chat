from .remote import *

# HOSTS CONFIGURATION
# ------------------------------------------------------------------------------

ALLOWED_HOSTS = ['staging.chat.rubick.cz']

# STORAGE CONFIGURATION
# ------------------------------------------------------------------------------

AWS_STORAGE_BUCKET_NAME = 'chat123'
