from .remote import *

# HOSTS CONFIGURATION
# ------------------------------------------------------------------------------

ALLOWED_HOSTS = ['chat.rubick.cz']

# STORAGE CONFIGURATION
# ------------------------------------------------------------------------------

AWS_STORAGE_BUCKET_NAME = 'chat456'
