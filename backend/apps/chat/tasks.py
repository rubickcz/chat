from celery import shared_task

from .models import Message


@shared_task
def save_message(nick, text):
    Message(nick=nick, text=text).save()
