from django.conf import settings
from django.db import models
from django.utils.translation import ugettext as _


class Message(models.Model):

    nickname = models.TextField()
    text = models.TextField()

    def dummy(self):
        return _('dummy')
