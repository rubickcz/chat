from django.test import SimpleTestCase

from apps.chat.models import Message


class MessageTestCase(SimpleTestCase):

    def test_dummy(self):
        self.assertEqual(Message().dummy(), 'dummy')
