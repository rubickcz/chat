import socket

from rest_framework import serializers

from .models import Message
from .tasks import save_message


class MessageSerializer(serializers.ModelSerializer):

    hostname = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = ('nick', 'text', 'hostname')

    def get_hostname(self, obj):
        return socket.gethostname().upper()

    def create(self, *args, **kwargs):
        save_message.delay(self.validated_data['nick'], self.validated_data['text'])
        return Message(**self.validated_data)

