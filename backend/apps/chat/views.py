from random import random
import time

from rest_framework import viewsets

from .serializers import MessageSerializer
from .models import Message


class MessageViewSet(viewsets.ModelViewSet):

    queryset = Message.objects.all().order_by('-pk')
    serializer_class = MessageSerializer

    def list(self, *args, **kwargs):
        time.sleep(0.5)
        return super().list(*args, **kwargs)
