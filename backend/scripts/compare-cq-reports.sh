#!/bin/sh

COMM_OUTPUT=$(comm -13 $@)
if [[ -n "$COMM_OUTPUT" ]]; then
  echo 'YOU HAVE INTRODUCED NEW CODE QUALITY ISSUES!'
  echo '--------------------------------------------'
  echo $COMM_OUTPUT
  exit 1
fi
