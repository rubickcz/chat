#!/bin/sh

BRANCH_COVERAGE=$(grep "TOTAL" backend/coverage_report.txt | sed -r 's/.* ([0-9]+\.[0-9]+)%$/\1/g')
MASTER_COVERAGE=$(curl $MASTER_COVERAGE_URL)

echo Branch: $BRANCH_COVERAGE
echo Master: $MASTER_COVERAGE

# todo put this in some common function which will vyblejt it in nice colors
if [[ ! -z $MASTER_COVERAGE ]] && [[ $(echo "$BRANCH_COVERAGE < $MASTER_COVERAGE" | bc) -eq 1 ]]; then
  echo ---------------------------------------------
  echo YOU HAVE DECREASED CODE COVERAGE!
  echo ---------------------------------------------
  exit 1
fi

if [[  $CI_COMMIT_REF_NAME = "master" ]]; then
    echo "Posting coverage to key value storage!"
    curl -X POST $MASTER_COVERAGE_URL/$BRANCH_COVERAGE
fi
