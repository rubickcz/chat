#!/bin/sh

IP=$1
IMAGE=$2
CONFIG=$3

apk add openssh-client
eval $(ssh-agent -s)
echo "$SSH_KEY" | ssh-add -
mkdir -p ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
scp docker-compose.deploy.yml root@$IP:~/
ssh root@$IP "
    export DJANGO_SETTINGS_MODULE=$CONFIG &&
    export IMAGE=$IMAGE &&
    docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com &&
    docker stack deploy -c docker-compose.deploy.yml --with-registry-auth chat
"
