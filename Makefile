PROJECT_NAME := chat
LOCAL_IMAGE := ${PROJECT_NAME}:local

image ?= ${LOCAL_IMAGE}

ifeq (${image}, ${LOCAL_IMAGE})
  mount_flag := --mount type=bind,source=$(shell pwd),target=/app
endif

build:
	docker build --cache-from ${image} -t ${image} .

test:
	export IMAGE=${image} && docker-compose -f docker-compose.test.yml up --abort-on-container-exit

code-quality:
	docker run ${mount_flag} ${image} pylama | tee ${report_file}

run:
	export IMAGE=${image} && \
	export DJANGO_SETTINGS_MODULE=settings.local && \
	docker-compose -f docker-compose.local.yml up

shell:
	docker exec -it $(shell docker container ls | grep _app | grep -Po ^[a-f0-9]+) bash
